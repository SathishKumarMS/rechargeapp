import Assets from '../assets';
export const OfferHelperArray = [
  {
    title: 'Recommented Packs',
    data: [
      {
        amount: 500,
        limit: 'Unlimited',
        speed: '100 Mbps',
        validity: 1,
        offer: true,
      },
      {
        amount: 249,
        limit: 'Unlimited',
        speed: '50 Mbps',
        validity: 1,
        offer: false,
      },
      {
        amount: 1499,
        limit: 'Unlimited',
        speed: '200 Mbps',
        validity: 3,
        offer: true,
      },
    ],
  },
  {
    title: 'Cricket Packs',
    data: [
      {
        amount: 500,
        limit: 'Unlimited',
        speed: '100 Mbps',
        validity: 1,
        offer: true,
      },
      {
        amount: 249,
        limit: 'Unlimited',
        speed: '50 Mbps',
        validity: 1,
        offer: false,
      },
      {
        amount: 1499,
        limit: 'Unlimited',
        speed: '200 Mbps',
        validity: 3,
        offer: true,
      },
    ],
  },
  {
    title: 'Truly Unlimited',
    data: [
      {
        amount: 500,
        limit: 'Unlimited',
        speed: '100 Mbps',
        validity: 1,
        offer: true,
      },
      {
        amount: 249,
        limit: 'Unlimited',
        speed: '50 Mbps',
        validity: 1,
        offer: true,
      },
      {
        amount: 1499,
        limit: 'Unlimited',
        speed: '200 Mbps',
        validity: 3,
        offer: false,
      },
    ],
  },
  {
    title: 'Basics',
    data: [
      {
        amount: 500,
        limit: 'Unlimited',
        speed: '100 Mbps',
        validity: 1,
        offer: true,
      },
      {
        amount: 249,
        limit: 'Unlimited',
        speed: '50 Mbps',
        validity: 1,
        offer: false,
      },
      {
        amount: 1499,
        limit: 'Unlimited',
        speed: '200 Mbps',
        validity: 3,
        offer: true,
      },
    ],
  },
];

export const recommentedpayment = [
  {
    btn: false,
    name: 'Recommended',
  },
  {
    btn: true,
    icon: Assets.icici,
    name: 'ICICI Bank Credit Card',
    number: 'XXXX-XXXX-XXXX-1234',
  },
  {
    btn: true,
    icon: Assets.airtel,
    name: 'Airtel Payments Bank',
    number: 10.6,
    balance: 'Balance: ',
    chargeContent: 'minimum recharge required ₹ 246',
    offers: true,
    offerContent: 'Get up to RS. 80 Cashback',
  },
  {
    btn: true,
    icon: Assets.upi,
    name: 'Airtel Pay',
    changeUPI: 'Change Bank A/c',
    number: 'Linked to PAYTM a/c no 8017',
    offers: true,
    offerContent: 'Flat Rs.20 Cashback',
  },
  {
    btn: true,
    icon: Assets.amazon,
    name: 'Amazon Pay',
    number: 690.0,
    balance: 'Balance: ',
    chargeContent: 'pay ₹589 using balance',
    offers: false,
  },
];

export const paymentType = [
  {
    payment: 'UPI',
    icon: Assets.amazon,
    upi: [
      {
        icon: Assets.airtel,
        name: 'Airtel',
        offer: true,
        offerContent: 'Flat Rs.20 Cashback',
      },
      {
        icon: Assets.phonepay,
        name: 'PhonePe',
      },
      {
        icon: Assets.googlepay,
        name: 'Google Pay',
      },
      {
        name: 'More options',
        others: true,
      },
    ],
  },
  {
    payment: 'CREDIT/DEBIT/ATM CARD',
    icon: Assets.amazon,
    card: true,
    upi: [
      {
        icon: Assets.airtel,
        name: 'Airtel',
        offer: true,
        offerContent: 'Flat Rs.20 Cashback',
      },
      {
        icon: Assets.phonepay,
        name: 'PhonePe',
      },
      {
        icon: Assets.googlepay,
        name: 'Google Pay',
      },
    ],
  },
  {
    payment: 'WALLET',
    icon: Assets.amazon,
    upi: [
      {
        icon: Assets.paytm,
        name: 'Paytm',
        linkpay: ' link & pay',
      },
      {
        icon: Assets.amazon,
        name: 'Amazon pay',
        number: 690.0,
        balance: 'Balance: ',
        chargeContent: 'pay ₹589 using balance',
      },
      {
        icon: Assets.airtel,
        name: 'Airtel Payments Bank',
        number: 10.6,
        balance: 'Balance: ',
        chargeContent: 'minimum recharge required ₹ 246',
        offer: true,
        offerContent: 'Get up to RS. 80 Cashback',
      },
    ],
  },
  {
    payment: 'NET BANKING',
    icon: Assets.amazon,
    upi: [
      {
        icon: Assets.sbi,
        name: 'SBI',
      },
      {
        icon: Assets.hdfc,
        name: 'HDFC',
      },
      {
        icon: Assets.icici,
        name: 'ICICI',
      },
      {
        name: 'More options',
        others: true,
      },
    ],
  },
];


export const filterData = [
  {
    amount: '500',
    limit: 'Unlimited',
    speed: '100',
    validity: 2,
    offer: true,
  },
  {
    amount: '249',
    limit: 'Unlimited',
    speed: '500',
    validity: 3,
    offer: false,
  },
  {
    amount: '1499',
    limit: 'Unlimited',
    speed: '200',
    validity: 3,
    offer: false,
  },
  {
    amount: '500',
    limit: 'Unlimited',
    speed: '100',
    validity: 1,
    offer: true,
  },
  {
    amount: '249',
    limit: 'Unlimited',
    speed: '300',
    validity: 1,
    offer: false,
  },
  {
    amount: '1499',
    limit: 'Unlimited',
    speed: '400',
    validity: 3,
    offer: false,
  },
]
