import {StyleSheet} from 'react-native';
import Colors from './Colors';

// Flex Styles
export const flex = flexValue => {
  return {
    flex: flexValue,
  };
};

export const flexGrow = flexValue => {
  return {
    flexGrow: flexValue,
  };
};

export const flexDirection = flexDirectionValue => {
  return {
    flexDirection: flexDirectionValue,
  };
};

// Image Styles
export const imageView = (heightValue, widthValue) => {
  return {
    height: heightValue,
    width: widthValue,
  };
};

// Other Styles
export const justifyContentCenteredView = justifyContentValue => {
  return {
    justifyContent: justifyContentValue,
    alignItems: 'center',
  };
};

export const justView = (styleLabel, styleValue) => {
  return {
    [styleLabel]: styleValue,
  };
};

export const margin = (marginHorizontalValue, marginVerticalValue) => {
  return {
    marginHorizontal: marginHorizontalValue,
    marginVertical: marginVerticalValue,
  };
};

export const padding = (paddingHorizontalValue, paddingVerticalValue) => {
  return {
    paddingHorizontal: paddingHorizontalValue,
    paddingVertical: paddingVerticalValue,
  };
};

// Screen Styles
export const screenContainer = backgroundColorValue => {
  return {
    flex: 1,
    backgroundColor: backgroundColorValue,
  };
};

export const screenSubContainer = {
  marginHorizontal: 16,
  marginVertical: 16,
};

export const border = (borderWidth, borderColor, borderRadius) => {
  return {
    borderWidth: borderWidth,
    borderColor: borderColor,
    borderRadius: borderRadius,
  };
};

// Text Styles
export const textView = (
  sizeValue,
  weightValue,
  colorValue,
  alignValue,
  textTransformValue,
) => {
  return {
    color: colorValue,
    fontFamily: 'Proxima Nova',
    fontSize: sizeValue,
    fontWeight: weightValue,
    textAlign: alignValue,
    textTransform: textTransformValue,
  };
};

//Sperator lines
export const speratorLine = {
  marginVertical: 8,
  borderBottomColor: Colors.dimGray,
  borderBottomWidth: StyleSheet.hairlineWidth,
};
