const colors = {
  white: '#FFFFFF',
  black: '#000000',
  blue: '#0000ff',
  shadow: '#333333',
  skyBlue: '#0097FF',
  quartz: '#DEDEE1',
  triadic: '#C4C9CF',
  gray: '#808080',
  raven: '#6C6D6F',
  mako: '#525353',
  spunpearl: '#AEAEAF',
  link: '#C4C9CF',
  cornflowerblue: '#468EF7',
  dimGray: '#737373',
  solitude: '#EAEBEC',
  aliceblue: '#F6FBFF',
};

export default colors;
