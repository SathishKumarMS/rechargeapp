import {Dimensions} from 'react-native';
// Get Screen Height
export const screenHeight = Dimensions.get('screen').height;

// Get Screen Weight
export const screenWidth = Dimensions.get('screen').width;

// Get Window Height
export const windowHeight = Dimensions.get('window').height;

// Get Window Weight
export const windowWidth = Dimensions.get('window').width;