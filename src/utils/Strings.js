const strings = {
  //PlaceHolders
  enterNumberPlaceHodlers: 'Enter airtel Xstream id or mobile number',
  addName: 'Xstream Premium',
  viewDetails: 'View details',
  giftContent: 'Save 140rs/m vs. monthly pack',
  offer: 'Only RS. 800/mo.',
  selectOffer: 'Selecte offers, Save more',
  amountPay: 'amount payable',
  selectPaymentOption: 'Select payment Options',
  searchNumber: 'Search Number...',
  speed: 'Speed',
  month: 'Month',
  validity: 'Validity',
  apply: 'Apply',
  safeString: 'Your money is always safe',
  secure:'100% secure payments',
  viewDetails: 'View Details',
};

export default strings;
