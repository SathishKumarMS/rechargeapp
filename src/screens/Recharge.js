import React from 'react';
import {
  FlatList,
  TouchableOpacity,
  View,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {
  dataPackes,
  phoneNumber,
  selectContact,
} from '../redux/actions/RechargeAction';
import Colors from '../utils/Colors';
import CustomTextInput from '../components/CustomTextInput';
import Contacts from 'react-native-contacts';
import CustomScroll from './CustomScroll';
import CustomText from '../components/CustomText';
import OfferCard from '../components/appComponents/OfferCard';
import Filter from '../components/Filter';
import MobileNumber from '../components/appComponents/MobileNumber';
import Icon from 'react-native-vector-icons/FontAwesome';
import Styles from '../styles/appStyles/RechargeStyle';
import * as Static from '../utils/Static';
import * as HelperStyles from '../utils/HelperStyles';

class Recharge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hideFilter: false,
      searchNumber: this.props.selectContact,
      filterValue: [],
      phoneData: [],
      filterData: Static.filterData,
      searchEnable: false,
    };
  }

  renderHeaderRight = () => {
    return (
      <TouchableOpacity
        style={[
          HelperStyles.padding(4, 4),
          HelperStyles.justifyContentCenteredView('center'),
          HelperStyles.imageView(40, 40),
          // Styles.headerRightIcon,
          {
            backgroundColor: Colors.raven,
            borderRadius: 40 / 2,
          },
        ]}>
        <Icon name="question-circle-o" size={22} color={Colors.white} />
      </TouchableOpacity>
    );
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerrLeft: () => this.renderHeaderRight(),
    });
  }

  componentDidMount() {
    // this.requestContactsPermission();
    const unsubscribe = this.props.navigation.addListener('focus', () => {
      this.setState({
        searchNumber: this.props.selectContact,
      });
    });

    return unsubscribe;
  }

  requestContactsPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the Contacts');
        this.getPermission();
      } else {
        console.log('Contacts permission denied');
      }
    } catch (err) {
      console.log(err);
    }
  };

  getPermission = () => {
    Contacts.getAll().then(contacts => {
      contacts.slice(0, 10).map(lol => {
        this.mobileDataHandler(lol);
      });
    });
  };

  mobileDataHandler = data => {
    let helperArray = this.state.phoneData;

    let mobile = data.phoneNumbers[0].number.replace(/-/g, '');

    if (data.phoneNumbers[0].number.slice(0, 1) == '+') {
      mobile = mobile.substring(3);
    }

    data.phoneNumbers.length != 0 &&
      helperArray.push({
        name: data.displayName,
        number: mobile,
      });

    data.phoneNumbers.length != 0 &&
      this.setState({
        phoneData: helperArray,
      });

    data.phoneNumbers.length != 0 && this.props.phoneNumber(helperArray);

    this.props.navigation.navigate('Contacts');
  };

  Item = item => (
    <View
      style={[
        HelperStyles.flex(1),
        {marginTop: item.item.offer ? 30 : 10},
        HelperStyles.screenSubContainer,
      ]}>
      <OfferCard
        value={item.item}
        onPress={() => {
          this.props.dataPackes(item.item);
          if (Boolean(this.state.searchNumber)) {
            this.props.navigation.navigate('Payment');
          } else {
            Alert.alert('Alert', 'Please Select Contact', [
              {text: 'OK', onPress: () => console.log('Okay selected')},
            ]);
          }
        }}
      />
    </View>
  );

  getFilterValue = filterValue => {
    if (this.state.filterValue.includes(filterValue)) {
      let helperArray = this.state.filterValue;
      let itemIndex = helperArray.indexOf(filterValue);

      helperArray.splice(itemIndex, 1);

      this.setState({
        filterValue: [...helperArray],
      });
    } else {
      this.setState({
        filterValue: [...this.state.filterValue, filterValue],
      });
    }
  };

  findOffers = () => {
    let data = [...Static.filterData];

    data = data.filter(item =>
      this.state.filterValue.includes(String(item.speed).toLowerCase()),
    );

    this.setState({
      filterData: data,
    });
  };

  searchFilter = txt => {
    console.log(typeof txt);

    const results = Static.filterData.filter(
      lol =>
        String(lol.amount).toLowerCase().includes(String(txt).toLowerCase()) ||
        String(lol.speed).toLowerCase().includes(String(txt).toLowerCase()) ||
        String(lol.validity).toLowerCase().includes(String(txt).toLowerCase()),
    );

    this.setState({
      filterData: results,
    });
  };

  render() {
    return (
      <View style={HelperStyles.screenContainer(Colors.black)}>
        <View
          style={[
            HelperStyles.flex(0.2),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <MobileNumber
            updateMasterState={txt => {
              this.setState({
                searchNumber: txt,
              });

              this.props.contact(txt);
            }}
            openContacts={async () => {
              await this.requestContactsPermission();

              this.setState({
                phoneData: [],
              });
            }}
            value={this.state.searchNumber}
          />
        </View>

        <View style={[Styles.packsContainer, HelperStyles.flex(0.8)]}>
          <View style={Styles.tabContainer} />
          <View
            style={[
              HelperStyles.justifyContentCenteredView('center'),
              HelperStyles.justView('flexDirection', 'row'),
              {
                marginHorizontal: !this.state.hideFilter ? 16 : 0,
              },
            ]}>
            {!this.state.hideFilter && (
              <CustomTextInput
                updateMasterState={txt => {
                  this.searchFilter(txt);

                  this.setState({
                    searchEnable: Boolean(txt) ? true : false,
                  });
                }}
                placeholder={'Search for a plan '}
                textInputStyle={Styles.searchBarContainer}
              />
            )}
            <Filter
              onPress={() => {
                this.setState({
                  hideFilter: !this.state.hideFilter,
                  filterValue: [],
                });
              }}
            />
            {this.state.hideFilter && (
              <FlatList
                data={['100', '200', '300', '400']}
                showsHorizontalScrollIndicator={false}
                horizontal
                style={HelperStyles.justView('marginHorizontal', 10)}
                renderItem={({item, index}) => {
                  return (
                    <TouchableOpacity
                      onPress={async () => {
                        await this.getFilterValue(item);
                        await this.findOffers(item);
                      }}
                      style={[
                        HelperStyles.flex(1),
                        Styles.filterChildStyles,
                        HelperStyles.border(1, Colors.gray, 20),
                        HelperStyles.padding(20, 8),
                        {
                          backgroundColor: this.state.filterValue.includes(item)
                            ? Colors.black
                            : Colors.white,
                        },
                      ]}>
                      <CustomText
                        value={`${item} Mbps`}
                        colorValue={
                          this.state.filterValue.includes(item)
                            ? Colors.white
                            : Colors.gray
                        }
                      />
                    </TouchableOpacity>
                  );
                }}
              />
            )}
          </View>
          {this.state.filterValue == 0 && !this.state.searchEnable ? (
            <CustomScroll navigation={this.props.navigation} />
          ) : (
            <View
              style={[
                HelperStyles.flex(1),
                HelperStyles.justView('marginVertical', 10),
              ]}>
              <FlatList
                data={this.state.filterData}
                showsHorizontalScrollIndicator={false}
                style={HelperStyles.justView('marginHorizontal', 10)}
                renderItem={this.Item}
              />
            </View>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectContact: state.app.selectContact,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dataPackes: data => {
      dispatch(dataPackes(data));
    },
    phoneNumber: mobileNumber => {
      dispatch(phoneNumber(mobileNumber));
    },
    contact: contact => {
      dispatch(selectContact(contact));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Recharge);
