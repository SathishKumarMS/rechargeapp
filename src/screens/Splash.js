import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import analytics from '@react-native-firebase/analytics';
import crashlytics from '@react-native-firebase/crashlytics';

class Splash extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    setTimeout(() => {
      // this.props.navigation.navigate('Recharge');
      analytics().logEvent('purchase', {
        value: 19.99,
        currency: 'USD',
        transaction_id: 'T12345',
        items: [
          {
            item_id: 'I12345',
            item_name: 'My Item',
            item_category: 'Apparel',
            quantity: 1,
            price: 19.99,
          },
        ],
      });
    }, 1000);
  }

  onSignIn = async user => {
    crashlytics().log('User signed in.');
    // crashlytics().crash();
    await Promise.all([
      crashlytics().setUserId(user.uid),
      crashlytics().setAttribute('credits', String(user.credits)),
      crashlytics().setAttributes({
        role: 'admin',
        followers: '13',
        email: user.email,
        username: user.username,
      }),
    ]);
  };

  render() {
    return (
      <TouchableOpacity
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
        onPress={async () =>
          // await analytics().logEvent('ad_click', {
          //   ad_id: '12345',
          //   ad_name: 'my_ad',
          //   ad_type: 'banner'
          // })
          {
            // crashlytics().crash(),
            onSignIn({
              uid: 'Aa0Bb1Cc2Dd3Ee4Ff5Gg6Hh7Ii8Jj9',
              username: 'Joaquin Phoenix',
              email: 'phoenix@example.com',
              credits: 42,
            });
          }
        }>
        <Text>Splash</Text>
      </TouchableOpacity>
    );
  }
}

export default Splash;
