import React from 'react';
import {FlatList, TouchableOpacity, View} from 'react-native';
import {selectContact} from '../redux/actions/RechargeAction';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/AntDesign';
import Card from '../components/Card';
import Colors from '../utils/Colors';
import CustomText from '../components/CustomText';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as HelperStyles from '../utils/HelperStyles';
import CustomTextInput from '../components/CustomTextInput';
import Styles from '../styles/appStyles/ContactStyles';
import Lables from '../utils/Strings';

class Contacts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contactList: this.props.mobileNumber,
    };
  }

  componentDidMount() {
    this.props.navigation.setOptions({
      headerLeft: () => this.renderHeaderLeft(),
      headerStyle: {backgroundColor: Colors.white},
      headerTitleStyle: {color: Colors.black},
    });
  }

  renderHeaderLeft = () => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.goBack();
        }}
        style={[
          HelperStyles.justifyContentCenteredView('center'),
          HelperStyles.padding(4, 4),
          HelperStyles.imageView(40, 40),
          {
            backgroundColor: Colors.aliceblue,
            shadowColor: Colors.shadow,
            shadowOpacity: 0.4,
            shadowRadius: 16,
            elevation: 9,
            borderRadius: 40 / 2,
          },
        ]}>
        <Icon name="arrowleft" size={22} color={Colors.gray} />
      </TouchableOpacity>
    );
  };

  findMobileNumebr = number => {
    const phoneNumber = this.state.contactList.filter(lol =>
      String(lol.number).toLowerCase().includes(String(number).toLowerCase()),
    );

    this.setState({
      contactList: phoneNumber,
    });
  };

  mobileCard = ({item}) => {
    return (
      <Card
        containerStyle={Styles.mobileCard}
        onPress={() => {
          this.props.selectContact(item.number);
          this.props.navigation.goBack();
        }}>
        <Ionicons
          name="people-circle-sharp"
          size={40}
          color={Colors.gray}
          style={HelperStyles.justView('marginRight', 20)}
        />
        <View style={HelperStyles.flexDirection('column')}>
          <CustomText
            value={item.name}
            sizeValue={16}
            weightValue={'600'}
            colorValue={Colors.black}
          />
          <CustomText
            value={item.number}
            sizeValue={14}
            weightValue={'600'}
            colorValue={Colors.gray}
          />
        </View>
      </Card>
    );
  };

  render() {
    return (
      <View style={HelperStyles.screenContainer(Colors.white)}>
        <FlatList
          data={this.state.contactList}
          keyExtractor={(item, index) => index}
          renderItem={this.mobileCard}
          ListHeaderComponent={
            <CustomTextInput
              updateMasterState={txt => {
                console.log(txt);
                Boolean(txt)
                  ? this.findMobileNumebr(txt)
                  : this.setState({
                      contactList: this.props.mobileNumber,
                    });
              }}
              placeholder={Lables.searchNumber}
              keyboardType={'number-pad'}
              textInputStyle={Styles.textInputContainer}
            />
          }
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    mobileNumber: state.app.mobileNumber,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    selectContact: contact => {
      dispatch(selectContact(contact));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
