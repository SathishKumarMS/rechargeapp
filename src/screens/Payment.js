import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
} from 'react-native';
import CustomText from '../components/CustomText';
import Icon from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Card from '../components/Card';
import CreditCard from '../components/appComponents/CreditCard';
import Colors from '../utils/Colors';
import RadioButton from '../components/RadioButton';
import Styles from '../styles/appStyles/paymentStyles';
import Lables from '../utils/Strings';
import * as HelperStyles from '../utils/HelperStyles';
import * as Static from '../utils/Static';
import CustomButton from '../components/CustomButton';
import {connect} from 'react-redux';

class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectRecommended: null,
      selectUpi: null,
      expend: 0,
      dataPackes: this.props.dataPackes,
      mobileNumber: this.props.mobileNumber,
    };
  }

  componentDidMount() {
    this.props.navigation.setOptions({
      headerLeft: () => this.renderHeaderLeft(),
      headerStyle: {backgroundColor: Colors.white},
      headerTitleStyle: {color: Colors.black},
    });
  }

  renderHeaderLeft = () => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.goBack();
        }}
        style={[
          HelperStyles.justifyContentCenteredView('center'),
          HelperStyles.padding(4, 4),
          HelperStyles.imageView(40, 40),
          {
            backgroundColor: Colors.aliceblue,
            shadowColor: Colors.shadow,
            shadowOpacity: 0.4,
            shadowRadius: 16,
            elevation: 9,
            borderRadius: 40 / 2,
          },
        ]}>
        <Icon name="arrowleft" size={22} color={Colors.gray} />
      </TouchableOpacity>
    );
  };

  Separetor = () => {
    return <View style={[HelperStyles.speratorLine]} />;
  };

  paymentButton = () => {
    Alert.alert('Recharge', 'Recharge Successfully', [
      {text: 'OK', onPress: () => this.props.navigation.goBack()},
    ]);
  };

  render() {
    return (
      <View style={HelperStyles.screenContainer('#F1F7FC')}>
        <ScrollView
          contentContainerStyle={HelperStyles.flexGrow(1)}
          keyboardShouldPersistTaps={'handled'}>
          <View style={HelperStyles.screenSubContainer}>
            <Card disabled={true} containerStyle={Styles.payableContainer}>
              <View style={Styles.payableSubContainer}>
                <CustomText
                  value={Lables.amountPay}
                  sizeValue={16}
                  weightValue={'800'}
                  colorValue={Colors.black}
                />
                <CustomText
                  value={`${'₹'} ${this.state.dataPackes.amount}`}
                  sizeValue={16}
                  weightValue={'800'}
                  colorValue={Colors.black}
                />
              </View>
              <CustomText
                value={`${'Prepaind'} ${this.state.mobileNumber}`}
                sizeValue={12}
                weightValue={'600'}
                colorValue={Colors.gray}
              />
            </Card>
            <Card disabled={true} containerStyle={Styles.selectOfferContainer}>
              <View style={Styles.selectOfferSubContainer}>
                <MaterialIcons
                  name="local-offer"
                  size={22}
                  color={Colors.black}
                  style={HelperStyles.justView('marginRight', 20)}
                />
                <CustomText
                  value={Lables.selectOffer}
                  sizeValue={16}
                  weightValue={'800'}
                  colorValue={Colors.black}
                />
              </View>
              <Icon name="right" size={18} color={Colors.blue} />
            </Card>
            <CustomText
              value={Lables.selectPaymentOption}
              sizeValue={16}
              weightValue={'900'}
              colorValue={Colors.black}
              textContainerStyle={HelperStyles.justView('marginBottom', 20)}
            />
            <Card disabled={true} containerStyle={Styles.recommentContainer}>
              {Static.recommentedpayment.map((lol, index) => {
                return (
                  <View key={index}>
                    <View style={Styles.recommentSubContainer}>
                      {index == 0 ? (
                        <>
                          <Feather
                            name="star"
                            size={22}
                            color={Colors.black}
                            style={HelperStyles.justView('marginRight', 20)}
                          />
                          <CustomText
                            value={lol.name}
                            sizeValue={16}
                            weightValue={'800'}
                            colorValue={Colors.black}
                          />
                        </>
                      ) : (
                        <>
                          <RadioButton
                            value={index}
                            selected={this.state.selectRecommended}
                            outerCircleStyle={{marginRight: 20}}
                            onPress={() => {
                              this.setState({selectRecommended: index});
                            }}
                          />
                          <Image
                            resizeMode={'contain'}
                            source={lol.icon}
                            style={Styles.paymentImage}
                          />
                          <View style={HelperStyles.flexDirection('column')}>
                            <CustomText
                              value={lol.name}
                              sizeValue={16}
                              weightValue={'800'}
                              colorValue={Colors.black}
                            />
                            <Text
                              style={HelperStyles.textView(
                                12,
                                '600',
                                Colors.gray,
                                'left',
                                'none',
                              )}>
                              {lol?.balance}
                              {lol.number}
                            </Text>
                            {lol.chargeContent && (
                              <CustomText
                                value={lol.chargeContent}
                                sizeValue={12}
                                weightValue={'600'}
                                colorValue={Colors.gray}
                              />
                            )}
                            {lol.changeUPI && (
                              <CustomText
                                value={lol.changeUPI}
                                sizeValue={12}
                                weightValue={'600'}
                                colorValue={Colors.skyBlue}
                              />
                            )}
                          </View>
                        </>
                      )}
                    </View>
                    {lol.offers && (
                      <View style={Styles.couponContainer}>
                        <View style={HelperStyles.flexDirection('row')}>
                          <MaterialIcons
                            name="local-offer"
                            size={22}
                            color={Colors.black}
                            style={HelperStyles.justView('marginRight', 20)}
                          />
                          <CustomText
                            value={lol.offerContent}
                            sizeValue={12}
                            weightValue={'800'}
                            colorValue={Colors.black}
                          />
                        </View>
                        <CustomText
                          value={Lables.apply}
                          sizeValue={12}
                          weightValue={'600'}
                          colorValue={Colors.skyBlue}
                        />
                      </View>
                    )}
                    {Static.recommentedpayment.length - 1 == index || (
                      <this.Separetor />
                    )}
                  </View>
                );
              })}
            </Card>
            {Static.paymentType.map((lol, index) => {
              return (
                <Card
                  disabled={index == this.state.expend ? true : false}
                  onPress={() => {
                    this.setState({
                      expend: index,
                      selectUpi: null,
                    });
                  }}
                  key={index}
                  containerStyle={[
                    HelperStyles.padding(10, 20),
                    HelperStyles.flexDirection('column'),
                    {
                      borderRadius: index == this.state.expend ? 10 : 0,
                      marginVertical: index == this.state.expend ? 20 : 0,
                    },
                  ]}>
                  <View style={Styles.paymentTypeContainer}>
                    <View style={HelperStyles.flexDirection('row')}>
                      <Image
                        resizeMode={'contain'}
                        source={lol.icon}
                        style={Styles.paymentImage}
                      />
                      <CustomText
                        value={lol.payment}
                        sizeValue={14}
                        weightValue={'800'}
                        colorValue={Colors.black}
                      />
                    </View>
                    <Entypo
                      name={
                        index == this.state.expend
                          ? 'chevron-thin-up'
                          : 'chevron-thin-down'
                      }
                      size={22}
                      color={Colors.black}
                      style={HelperStyles.justView('marginRight', 20)}
                    />
                  </View>
                  {lol.card && index == this.state.expend ? (
                    <>
                      <CreditCard />
                    </>
                  ) : (
                    index == this.state.expend &&
                    lol.upi.map((lol, index) => {
                      return (
                        <View key={index} style={Styles.selectPayType}>
                          <View style={Styles.selectPayTypeSubContainer}>
                            {lol.others ? (
                              <MaterialIcons
                                name="dashboard"
                                size={22}
                                color={Colors.black}
                                style={HelperStyles.justView('marginRight', 20)}
                              />
                            ) : (
                              <RadioButton
                                value={index}
                                selected={this.state.selectUpi}
                                outerCircleStyle={HelperStyles.justView(
                                  'marginRight',
                                  20,
                                )}
                                onPress={() => {
                                  this.setState({selectUpi: index});
                                }}
                              />
                            )}
                            {lol.icon && (
                              <Image
                                resizeMode={'contain'}
                                source={lol.icon}
                                style={Styles.paymentImage}
                              />
                            )}
                            <View style={Styles.selectPayType}>
                              <CustomText
                                value={lol.name}
                                sizeValue={16}
                                weightValue={'800'}
                                colorValue={Colors.black}
                              />
                              {lol.balance && (
                                <CustomText
                                  value={`${lol.balance} ${lol.number}`}
                                  sizeValue={12}
                                  weightValue={'600'}
                                  colorValue={Colors.gray}
                                />
                              )}
                              {lol.balance && (
                                <CustomText
                                  value={lol.chargeContent}
                                  sizeValue={12}
                                  weightValue={'600'}
                                  colorValue={Colors.gray}
                                />
                              )}
                            </View>
                          </View>
                          {lol.offer && (
                            <View style={Styles.offerContainer}>
                              <View style={HelperStyles.flexDirection('row')}>
                                <MaterialIcons
                                  name="local-offer"
                                  size={22}
                                  color={Colors.black}
                                  style={HelperStyles.justView(
                                    'marginRight',
                                    20,
                                  )}
                                />
                                <CustomText
                                  value={lol.offerContent}
                                  sizeValue={12}
                                  weightValue={'800'}
                                  colorValue={Colors.black}
                                />
                              </View>
                              <CustomText
                                value={Lables.apply}
                                sizeValue={12}
                                weightValue={'600'}
                                colorValue={Colors.skyBlue}
                              />
                            </View>
                          )}
                        </View>
                      );
                    })
                  )}
                </Card>
              );
            })}
            <View style={HelperStyles.margin(10, 10)}>
              <CustomText
                value={Lables.safeString}
                sizeValue={16}
                weightValue={'600'}
                colorValue={Colors.black}
                alignValue={'center'}
                textContainerStyle={HelperStyles.justView('marginVertical', 5)}
              />
              <CustomText
                value={Lables.secure}
                sizeValue={14}
                weightValue={'600'}
                colorValue={Colors.gray}
                alignValue={'center'}
                textContainerStyle={HelperStyles.justView('marginVertical', 5)}
              />
            </View>
          </View>
        </ScrollView>
        <Card
          disabled={true}
          containerStyle={[
            HelperStyles.padding(16, 20),
            Styles.paymentButtonCard,
          ]}>
          <View style={[HelperStyles.flexDirection('column')]}>
            <CustomText
              value={`${'₹'} ${this.state.dataPackes.amount}`}
              sizeValue={14}
              weightValue={'800'}
              colorValue={Colors.black}
              textContainerStyle={HelperStyles.justView('marginVertical', 2)}
            />
            <TouchableOpacity>
              <CustomText
                value={Lables.viewDetails}
                sizeValue={12}
                weightValue={'600'}
                colorValue={Colors.skyBlue}
                textContainerStyle={HelperStyles.justView('marginVertical', 2)}
              />
            </TouchableOpacity>
          </View>
          <CustomButton
            lable={'PAY NOW'}
            onPress={() => {
              this.paymentButton();
            }}
            customContainerStyle={Styles.paynowButton}
            customTextStyle={[HelperStyles.justView('color', Colors.gray)]}
          />
        </Card>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataPackes: state.app.dataPackes,
    mobileNumber: state.app.selectContact,
  };
};

export default connect(mapStateToProps, null)(Payment);
