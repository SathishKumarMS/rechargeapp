import React from 'react';
import {
  FlatList,
  SectionList,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {dataPackes} from '../redux/actions/RechargeAction';
import Colors from '../utils/Colors';
import CustomText from '../components/CustomText';
import OfferCard from '../components/appComponents/OfferCard';
import * as Static from '../utils/Static';
import * as HelperStyles from '../utils/HelperStyles';

class CustomScroll extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      getIndex: 0,
    };
    this.scrollRef = React.createRef();
    this.offerRef = React.createRef();
  }

  Item = item => (
    <View
      style={[
        HelperStyles.flex(1),
        {marginTop: item.item.offer ? 30 : 10},
        HelperStyles.screenSubContainer,
      ]}>
      <OfferCard
        value={item.item}
        onPress={() => {
          this.props.dataPackes(item.item);
          if (Boolean(this.props.mobileNumber)) {
            this.props.navigation.navigate('Payment');
          } else {
            Alert.alert('Alert', 'Please Select Contact', [
              {text: 'OK', onPress: () => console.log('Okay')},
            ]);
          }
          // this.props.navigation.navigate('Payment');
        }}
      />
    </View>
  );

  updateRef = index => {
    this.setState({
      getIndex: index,
    });
    this.scrollRef.current.scrollToIndex({animated: true, index: index});
  };

  scrollHandler = e => {
    console.log('SCROLL EVENT IS HERE:::::::::::', e.nativeEvent.contentOffset);
    if (e.nativeEvent.contentOffset.y == 0) {
      this.updateRef(0);
    } else if (
      e.nativeEvent.contentOffset.y > 350 &&
      e.nativeEvent.contentOffset.y < 900
    ) {
      this.updateRef(1);
    } else if (
      e.nativeEvent.contentOffset.y > 900 &&
      e.nativeEvent.contentOffset.y < 1500
    ) {
      this.updateRef(2);
    } else if (
      e.nativeEvent.contentOffset.y > 1500 &&
      e.nativeEvent.contentOffset.y < 1800
    ) {
      this.updateRef(3);
    }
  };

  onScrolling = index => {
    console.log(index);

    this.offerRef.current.scrollToLocation({
      animated: true,
      viewPosition:
        index === 0
          ? 0
          : index === 1
          ? 0.05
          : index === 2
          ? -1.95
          : index === 3
          ? -3.95
          : 0,
      sectionIndex:
        index === 0
          ? 0
          : index === 1
          ? 0.05
          : index === 2
          ? 0.08
          : index === 3
          ? 0.09
          : 0,
      itemIndex:
        index === 0
          ? 0
          : index === 1
          ? 0.05
          : index === 2
          ? 0.08
          : index === 3
          ? 0.09
          : 0,
    });

    this.setState({
      getIndex: index,
    });

    this.scrollRef.current.scrollToIndex({
      animated: true,
      index: index,
    });
  };

  // index === 0
  // ? 0
  // : index === 1
  // ? 0.05
  // : index === 2
  // ? 0.1
  // : index === 3
  // ? 0.2
  // : null,

  render() {
    return (
      <View style={{flex: 1}}>
        <FlatList
          data={[
            'Recommented Packs',
            'Cricket Packs',
            'Truly Unlimited',
            'Basics',
          ]}
          ref={this.scrollRef}
          showsHorizontalScrollIndicator={false}
          horizontal
          renderItem={({item, index}) => {
            return (
              <View
                style={[
                  HelperStyles.flex(1),
                  HelperStyles.justView('marginVertical', 20),
                ]}>
                <TouchableOpacity
                  style={HelperStyles.justView('marginHorizontal', 20)}
                  onPress={() => this.onScrolling(index)}>
                  <CustomText
                    value={item}
                    sizeValue={16}
                    weightValue={'600'}
                    colorValue={Colors.gray}
                  />
                </TouchableOpacity>
                <View
                  style={[
                    HelperStyles.justView('height', 2),
                    HelperStyles.justView('marginTop', 10),
                    {
                      backgroundColor:
                        this.state.getIndex == index
                          ? Colors.black
                          : Colors.solitude,
                    },
                  ]}
                />
              </View>
            );
          }}
        />
        <SectionList
          sections={Static.OfferHelperArray}
          keyExtractor={(item, index) => item + index}
          renderItem={this.Item}
          onScroll={e => this.scrollHandler(e)}
          ref={this.offerRef}
          renderSectionHeader={({section: {title}}) => (
            <CustomText
              value={title}
              sizeValue={16}
              weightValue={'600'}
              colorValue={Colors.gray}
              textContainerStyle={{marginHorizontal: 16}}
            />
          )}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    mobileNumber: state.app.selectContact,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dataPackes: data => {
      dispatch(dataPackes(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomScroll);
