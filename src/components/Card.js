import React from 'react';
import {TouchableOpacity} from 'react-native';
import Styles from '../styles/componentStyles/CardStyles';
import Colors from '../utils/Colors';
import * as HelperStyles from '../utils/HelperStyles';

class Card extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {disabled, containerStyle = {}, children} = this.props;
    return (
      <TouchableOpacity
        disabled={disabled}
        onPress={() => {
          this.props.onPress();
        }}
        style={[
          HelperStyles.justView('backgroundColor', Colors.white),
          Styles.cardContainer,
          containerStyle,
        ]}>
        {children}
      </TouchableOpacity>
    );
  }
}

export default Card;
