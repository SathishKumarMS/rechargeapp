import React from 'react';
import {Text, View} from 'react-native';
import Colors from '../utils/Colors';
import * as HelperStyles from '../utils/HelperStyles';

class CustomText extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      sizeValue = 16,
      weightValue = '600',
      colorValue = Colors.gray,
      alignValue = 'left',
      textTransformValue = 'none',
      value = null,
      textContainerStyle = {},
    } = this.props;
    return (
      <Text
        style={[
          HelperStyles.textView(
            sizeValue,
            weightValue,
            colorValue,
            alignValue,
            textTransformValue,
          ),
          textContainerStyle,
        ]}>
        {value}
      </Text>
    );
  }
}

export default CustomText;
