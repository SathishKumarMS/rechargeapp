import React from 'react';
import {View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Colors from '../../utils/Colors';
import RecommendedPackes from '../../screens/RecommendedPacks';
import CricketPacks from '../../screens/CricketPacks';
import TrulyUnlimited from '../../screens/TrulyUnlimited';
import TabBar from '../TabBar';
import * as HelperStyles from '../../utils/HelperStyles';

const TopTabs = createMaterialTopTabNavigator();

class SchemeTab extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={HelperStyles.screenContainer(Colors.white)}>
        <TopTabs.Navigator
          animationEnabled={true}
          initialRouteName={'Recommended Packes'}
          backBehavior={'history'}
          screenOptions={{swipeEnabled: false}}
          tabBar={props => (
            <TabBar
              {...props}
              horizontalScrollView={true}
              getCurrentRoute={routeName => {
                console.log(routeName)
              }}
              type={'outlined'}
            />
          )}
          style={[
            HelperStyles.flex(1),
            HelperStyles.justView('backgroundColor', Colors.white),
          ]}>
          <TopTabs.Screen
            name="Recommended Packes"
            component={RecommendedPackes}
          />
          <TopTabs.Screen name="Cricket Packs" component={CricketPacks} />
          <TopTabs.Screen name="Truly Unlimited" component={TrulyUnlimited} />
        </TopTabs.Navigator>
      </View>
    );
  }
}

export default SchemeTab;
