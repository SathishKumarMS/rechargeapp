import React, {Component} from 'react';
import {View, Image, TouchableWithoutFeedback} from 'react-native';
import Assets from '../../assets';
import CustomText from '../CustomText';
import LinearGradient from 'react-native-linear-gradient';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Lables from '../../utils/Strings';
import * as HelperStyles from '../../utils/HelperStyles';
import Styles from '../../styles/componentStyles/OfferCardStyle';
import Colors from '../../utils/Colors';

export default class OfferCard extends Component {
  constructor(props) {
    super(props);
  }

  renderOffers = value => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.props.onPress();
        }}>
        <View
          style={{
            borderWidth: this.props.value.offer ? 0 : 1,
            borderColor: this.props.value.offer ? Colors.white : Colors.gray,
            borderRadius: 10,
          }}>
          <View style={Styles.offerCardContainer}>
            <View style={Styles.offerCardSubContainer}>
              <View style={Styles.card}>
                <CustomText
                  value={`${'₹'} ${this.props.value.amount}`}
                  sizeValue={14}
                  weightValue={'800'}
                  colorValue={Colors.black}
                />
                <CustomText
                  value={value.limit}
                  sizeValue={14}
                  weightValue={'800'}
                  colorValue={Colors.gray}
                />
              </View>
              <View style={Styles.card}>
                <CustomText
                  value={value.speed}
                  sizeValue={14}
                  weightValue={'800'}
                  colorValue={Colors.black}
                />
                <CustomText
                  value={Lables.speed}
                  sizeValue={14}
                  weightValue={'700'}
                  colorValue={Colors.gray}
                />
              </View>
              <View style={Styles.card}>
                <View style={Styles.validatyCard}>
                  <CustomText
                    value={`${value.validity} ${Lables.month}`}
                    sizeValue={14}
                    weightValue={'800'}
                    colorValue={Colors.black}
                  />
                  <Entypo
                    name="chevron-right"
                    size={18}
                    color="#468EF7"
                    style={HelperStyles.justView('marginHorizontal', 6)}
                  />
                </View>
                <CustomText
                  value={Lables.validity}
                  sizeValue={14}
                  weightValue={'700'}
                  colorValue={Colors.gray}
                />
              </View>
            </View>
            <View style={HelperStyles.speratorLine} />
            <View style={Styles.viewDetaileContainer}>
              <View style={Styles.addContainer}>
                <Image
                  resizeMode={'contain'}
                  source={Assets.xstream}
                  style={Styles.addImageStyle}
                />
                <CustomText
                  value={Lables.addName}
                  sizeValue={14}
                  weightValue={'700'}
                  colorValue={Colors.gray}
                />
              </View>
              <CustomText
                value={Lables.viewDetails}
                sizeValue={14}
                weightValue={'700'}
                colorValue={Colors.gray}
              />
            </View>
          </View>
          {this.props.value.offer && (
            <View style={Styles.offerContainer}>
              <AntDesign
                name="gift"
                size={18}
                color={Colors.white}
                style={HelperStyles.justView('marginHorizontal', 6)}
              />
              <CustomText
                value={Lables.giftContent}
                sizeValue={12}
                weightValue={'700'}
                colorValue={Colors.white}
              />
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    const {value} = this.props;
    return value.offer ? (
      <LinearGradient
        colors={[
          '#00FFFF',
          '#17C8FF',
          '#329BFF',
          '#4C64FF',
          '#6536FF',
          '#8000FF',
        ]}
        start={{x: 0, y: 1}}
        end={{x: 1, y: 1}}
        style={[HelperStyles.justView('borderRadius', 10)]}>
        {this.renderOffers(value)}

        <LinearGradient
          colors={[
            '#00FFFF',
            '#17C8FF',
            '#329BFF',
            '#4C64FF',
            '#6536FF',
            '#8000FF',
          ]}
          start={{x: 0, y: 1}}
          end={{x: 1, y: 1}}
          style={Styles.offer}>
          <CustomText
            value={Lables.offer}
            sizeValue={12}
            weightValue={'700'}
            colorValue={Colors.white}
          />
        </LinearGradient>
      </LinearGradient>
    ) : (
      this.renderOffers(value)
    );
  }
}
