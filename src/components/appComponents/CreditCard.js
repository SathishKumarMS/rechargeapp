import React from 'react';
import {View} from 'react-native';
import CustomTextInput from '../CustomTextInput';
import * as HelperStyles from '../../utils/HelperStyles';
import Colors from '../../utils/Colors';
import Styles from '../../styles/componentStyles/CreditCardStyles';
class CreditCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <View style={HelperStyles.justView('marginVertical', 16)}>
          <CustomTextInput
            showicon={true}
            maxLength={16}
            keyboardType={'number-pad'}
            placeholder={'Card Number'}
            textInputStyle={Styles.creditCardContainer}
          />
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginVertical: 16}}>
          <View style={{flex: 0.6, marginRight: 16}}>
            <CustomTextInput
              maxLength={4}
              keyboardType={'number-pad'}
              placeholder={'Valid thru (mm/dd)'}
              textInputStyle={Styles.creditCardContainer}
            />
          </View>
          <View style={{flex: 0.4}}>
            <CustomTextInput
              showicon={true}
              maxLength={3}
              keyboardType={'number-pad'}
              placeholder={'CVV'}
              isPassword={true}
              textInputStyle={Styles.creditCardContainer}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default CreditCard;
