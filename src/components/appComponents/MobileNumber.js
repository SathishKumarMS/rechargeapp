import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Labels from '../../utils/Strings';
import CustomTextInput from '../CustomTextInput';
import Colors from '../../utils/Colors';
import Styles from '../../styles/componentStyles/MobileNumberStyles';
import * as HelperStyles from '../../utils/HelperStyles';
import CustomText from '../CustomText';

class MobileNumber extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      updateMasterState = () => {},
      openContacts = () => {},
      value = null,
    } = this.props;
    return (
      <View style={Styles.mobileNumberContainer}>
        <View
          style={[
            HelperStyles.flex(0.9),
            HelperStyles.justView('flexDirection', 'column'),
          ]}>
          <CustomText
            value={Labels.enterNumberPlaceHodlers}
            sizeValue={12}
            colorValue={Colors.spunpearl}
            textContainerStyle={HelperStyles.justView('marginVertical', 4)}
          />
          <CustomTextInput
            keyboardType={'number-pad'}
            updateMasterState={txt => {
              updateMasterState(txt);
            }}
            maxLength={10}
            value={value}
            textInputStyle={{color: Colors.spunpearl}}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            openContacts();
          }}
          style={[
            HelperStyles.flex(0.1),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <AntDesign name="contacts" size={26} color={Colors.spunpearl} />
        </TouchableOpacity>
      </View>
    );
  }
}

export default MobileNumber;
