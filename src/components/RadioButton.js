import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import Colors from '../utils/Colors';
import Styles from '../styles/componentStyles/RadioButtonStyles';
import * as HelperStyles from '../utils/HelperStyles';

class RadioButton extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onPress();
        }}
        style={[
          Styles.outerCircleContainer(20),
          HelperStyles.justView('borderColor', Colors.skyBlue),
          this.props.outerCircleStyle,
        ]}>
        {this.props.value == this.props.selected && (
          <View
            style={[
              Styles.innerCircleContainer(20 / 2),
              this.props.innerCircleStyle,
            ]}
          />
        )}
      </TouchableOpacity>
    );
  }
}

export default RadioButton;
