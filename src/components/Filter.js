import React from 'react';
import {TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../utils/Colors';
import * as HelperStyles from '../utils/HelperStyles';

class Filter extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {onPress = () => {}} = this.props;
    return (
      <TouchableOpacity
        onPress={() => {
          onPress();
        }}
        style={[
          HelperStyles.justifyContentCenteredView('center'),
          HelperStyles.padding(4, 4),
          HelperStyles.imageView(40, 40),
          HelperStyles.justView('backgroundColor', Colors.white),
          HelperStyles.border(1, Colors.link, 10),
          HelperStyles.justView('marginLeft', 16),
        ]}>
        <AntDesign name="filter" size={22} color={Colors.cornflowerblue} />
      </TouchableOpacity>
    );
  }
}

export default Filter;
