import React from 'react';
import {TextInput, TouchableOpacity, View} from 'react-native';
import Colors from '../utils/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Styles from '../styles/componentStyles/CustomTextInputStyles';
import * as HelperStyles from '../utils/HelperStyles';

class CustomTextInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {passwardVisibility: this.props.isPassword};
  }

  render() {
    const {
      autoCapitalize = 'none',
      editable = true,
      keyboardType = 'default',
      maxLength = null,
      placeholder = '',
      isPassword = false,
      textInputStyle = {},
      updateMasterState = () => {},
      onFocus,
      value = null,
    } = this.props;
    return (
      <View
        style={[
          HelperStyles.flex(1),
          HelperStyles.flexDirection('row'),
          HelperStyles.justView('justifyContent', 'space-between'),
        ]}>
        <TextInput
          autoCapitalize={autoCapitalize}
          editable={editable}
          keyboardType={keyboardType}
          maxLength={maxLength}
          placeholder={placeholder}
          secureTextEntry={this.state.passwardVisibility}
          onFocus={onFocus}
          onChangeText={txt => {
            updateMasterState(txt);
          }}
          style={[
            Styles.textInputContainer,
            HelperStyles.flex(1),
            HelperStyles.justView('color', Colors.black),
            textInputStyle,
          ]}
          value={value}
        />
        {this.props.showicon && (
          <View style={Styles.iconContainer}>
            {isPassword ? (
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    passwardVisibility: !this.state.passwardVisibility,
                  });
                }}>
                <MaterialIcons
                  name={
                    this.state.passwardVisibility
                      ? 'visibility'
                      : 'visibility-off'
                  }
                  size={24}
                  color={'gray'}
                  style={HelperStyles.justView('top', 4)}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={() => {}}>
                <AntDesign
                  name={'closecircleo'}
                  size={24}
                  color={'gray'}
                  style={HelperStyles.justView('top', 4)}
                />
              </TouchableOpacity>
            )}
          </View>
        )}
      </View>
    );
  }
}

export default CustomTextInput;
