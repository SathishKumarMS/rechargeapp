import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Colors from '../utils/Colors';
import Styles from '../styles/componentStyles/CustomButtonStyles';
import * as HelperStyles from '../utils/HelperStyles';

class CustomButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      disabled = false,
      lable,
      customContainerStyle = {},
      customTextStyle = {},
      onPress = () => {},
    } = this.props;
    return (
      <TouchableOpacity
        disabled={disabled}
        style={[Styles.buttonContainer, customContainerStyle]}
        onPress={() => {
          onPress();
        }}>
        <View
          style={[
            HelperStyles.flex(1),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <Text
            style={
              ([
                HelperStyles.textView(
                  16,
                  '600',
                  Colors.white,
                  'center',
                  'capitalize',
                ),
              ],
              customTextStyle)
            }>
            {lable}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default CustomButton;
