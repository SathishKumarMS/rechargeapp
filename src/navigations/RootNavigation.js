import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Splash from '../screens/Splash';
import Recharge from '../screens/Recharge';
import Payment from '../screens/Payment';
import Contacts from '../screens/Contacts';
import Colors from '../utils/Colors';
import messaging from '@react-native-firebase/messaging';

const Stack = createNativeStackNavigator();

class RootNavigation extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.getFCMToken()
  }

  getFCMToken = async () => {
    await messaging().registerDeviceForRemoteMessages();
    const token = await messaging().getToken();

    console.log('fcm token:::::::::::::::', token)
  }

  render() {
    return (
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={({navigation}) => ({
          headerBackTitleVisible: false,
          headerTitleAlign: 'center',
          headerTitleStyle: {color: Colors.white},
          headerShadowVisible: false,
          headerBackVisible: false,
          headerStyle: {backgroundColor: Colors.black},
        })}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Recharge" component={Recharge} />
        <Stack.Screen name="Payment" component={Payment} />
        <Stack.Screen name="Contacts" component={Contacts} />
      </Stack.Navigator>
    );
  }
}

export default RootNavigation;
