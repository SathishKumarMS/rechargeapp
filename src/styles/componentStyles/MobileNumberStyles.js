import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as Helpers from '../../utils/Helpers';

const Styles = StyleSheet.create({
  mobileNumberContainer: {
    width: Helpers.screenWidth * 0.9,
    height: 70,
    backgroundColor: Colors.mako,
    borderWidth: 1,
    borderRadius: 20,
    paddingHorizontal: 20,
    borderColor: Colors.spunpearl,
    flexDirection: 'row',
  }
});

export default Styles;
