import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as HelperStyles from '../../utils/HelperStyles';

const Styles = StyleSheet.create({
  creditCardContainer: [
    HelperStyles.border(1, Colors.link, 10),
    HelperStyles.imageView(60, 0),
  ],
});

export default Styles;
