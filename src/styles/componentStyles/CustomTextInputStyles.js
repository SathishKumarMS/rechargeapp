import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
 textInputContainer: {
    fontSize: 16,
    fontWeight: '400',
    paddingHorizontal: 6,
  },
  iconContainer: {position: 'absolute', right: 10, top: 14}
});

export default Styles;
