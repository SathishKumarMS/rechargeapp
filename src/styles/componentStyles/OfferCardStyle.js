import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as HelperStyles from '../../utils/HelperStyles';

const Styles = StyleSheet.create({
  offerCardContainer: [
    HelperStyles.padding(2, 2),
    HelperStyles.margin(3, 3),
    HelperStyles.justView('center'),
    HelperStyles.justView('borderRadius', 10),
    HelperStyles.flex(1),
    HelperStyles.justView('backgroundColor', Colors.white),
  ],
  offerCardSubContainer: [
    HelperStyles.flex(1),
    HelperStyles.flexDirection('row'),
    HelperStyles.justifyContentCenteredView('space-between'),
    HelperStyles.justView('paddingHorizontal', 16),
  ],
  card: {flexDirection: 'column', marginVertical: 16},
  validatyCard: [
    HelperStyles.flexDirection('row'),
    HelperStyles.justifyContentCenteredView('center'),
  ],
  viewDetaileContainer: [
    HelperStyles.flexDirection('row'),
    HelperStyles.justView('justifyContent', 'space-between'),
    HelperStyles.margin(10, 10),
  ],
  addContainer: [
    HelperStyles.flexDirection('row'),
    HelperStyles.justifyContentCenteredView('center'),
  ],
  addImageStyle: [
    HelperStyles.imageView(24, 24),
    HelperStyles.justView('marginHorizontal', 10),
  ],
  offerContainer: [
    HelperStyles.flexDirection('row'),
    HelperStyles.justifyContentCenteredView('flex-start'),
    HelperStyles.margin(10, 6),
  ],
  offer: {
    padding: 6,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    position: 'absolute',
    top: -24,
  },
});

export default Styles;
