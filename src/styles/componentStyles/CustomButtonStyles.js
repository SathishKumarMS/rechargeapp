import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const Styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'stretch',
    paddingHorizontal: 8,
    paddingVertical: 8,
    elevation: 4,
  },
});

export default Styles;
