import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const Styles = StyleSheet.create({
  outerCircleContainer: outerRadius => {
    return {
      width: outerRadius,
      height: outerRadius,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 2,
      borderRadius: outerRadius / 2,
    };
  },
  innerCircleContainer: innerRadius => {
    return {
      width: innerRadius,
      height: innerRadius,
      backgroundColor: Colors.skyBlue,
      borderRadius: innerRadius / 2,
    };
  },
});

export default Styles;
