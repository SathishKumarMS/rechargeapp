import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as Helpers from '../../utils/Helpers';

const Styles = StyleSheet.create({
  packsContainer: {
    backgroundColor: Colors.white,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  tabContainer: {
    backgroundColor: Colors.quartz,
    width: Helpers.screenWidth * 0.15,
    height: Helpers.screenHeight * 0.01,
    borderRadius: 10,
    alignSelf: 'center',
    marginVertical: 16,
  },
  searchBarContainer: {
    height: 40,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: Colors.triadic,
    paddingHorizontal: 10,
  },

  filterChildStyles: {
    marginHorizontal: 10,
  },
  headerRightIcon: {
    backgroundColor: Colors.raven,
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
  },
});

export default Styles;
