import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as HelperStyles from '../../utils/HelperStyles';

const Styles = StyleSheet.create({
  backBtnStyle: {
    backgroundColor: Colors.aliceblue,
    shadowColor: Colors.shadow,
    shadowOpacity: 0.4,
    shadowRadius: 16,
    elevation: 9,
    borderRadius: 40 / 2,
  },
  payableContainer: [
    HelperStyles.padding(20, 20),
    HelperStyles.flexDirection('column'),
    HelperStyles.justView('marginBottom', 20),
  ],
  payableSubContainer: [
    HelperStyles.flex(1),
    HelperStyles.flexDirection('row'),
    HelperStyles.justView('justifyContent', 'space-between'),
    HelperStyles.justView('marginBottom', 6),
  ],
  selectOfferContainer: [
    HelperStyles.padding(20, 20),
    HelperStyles.justView('marginBottom', 20),
    HelperStyles.justView('justifyContent', 'space-between'),
  ],
  selectOfferSubContainer: [
    HelperStyles.flexDirection('row'),
    HelperStyles.justifyContentCenteredView('center'),
  ],
  recommentContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  recommentSubContainer: [
    HelperStyles.padding(20, 20),
    HelperStyles.flexDirection('row'),
  ],
  paymentImage: [
    HelperStyles.imageView(24, 24),
    HelperStyles.justView('marginHorizontal', 10),
  ],
  couponContainer: [
    HelperStyles.padding(20, 10),
    HelperStyles.border(1, 'red', 10),
    HelperStyles.flexDirection('row'),
    HelperStyles.justView('justifyContent', 'space-between'),
  ],
  paymentTypeContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  selectPayType: {flex: 1, flexDirection: 'column'},
  selectPayTypeSubContainer: [
    HelperStyles.margin(20, 20),
    {
      flex: 1,
      flexDirection: 'row',
    },
  ],
  offerContainer: [
    HelperStyles.padding(20, 10),
    HelperStyles.border(1, 'red', 10),
    HelperStyles.flexDirection('row'),
    HelperStyles.justView('justifyContent', 'space-between'),
  ],
  paymentButtonCard: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    justifyContent: 'space-between',
  },
  paynowButton: {
    backgroundColor: '#575456',
    width: '30%',
    height: 50,
    borderRadius: 16,
  },
});

export default Styles;
