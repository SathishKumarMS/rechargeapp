import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as HelperStyles from '../../utils/HelperStyles';

const Styles = StyleSheet.create({
  textInputContainer: [
    HelperStyles.justView('height', 40),
    HelperStyles.padding(10, 0),
    HelperStyles.border(1, Colors.link, 10),
    HelperStyles.screenSubContainer,
  ],
  mobileCard: [
    HelperStyles.margin(20, 10),
    HelperStyles.justView('alignItems', 'center'),
  ],
});

export default Styles;
