import * as Types from '../Root.Types';

const initialState = {
  scrollKey: 0,
  selectContact: null,
  dataPackes: null,
  mobileNumber: null,
};

const RechargeReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.SCROLLKEY:
      return {...state, scrollKey: action.payload};

    case Types.SELECTCONTACT:
      return {...state, selectContact: action.payload};

    case Types.DATAPACKES:
      return {...state, dataPackes: action.payload};

    case Types.MOBILENUMBER:
      return {...state, mobileNumber: action.payload};
    default:
      return state;
  }
};

export default RechargeReducer;
