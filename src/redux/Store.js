import {createStore, compose, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import rootReducer from './Root.Reducers';

let composedEnhancer;

composedEnhancer = compose(applyMiddleware(logger));

export const initStore = () => createStore(rootReducer, {}, composedEnhancer);

const store = initStore();

export default store;
