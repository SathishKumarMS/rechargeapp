import * as Types from '../Root.Types';

export const scrollKey = key => {
  return {
    type: Types.SCROLLKEY,
    payload: key,
  };
};

export const selectContact = contacts => {
  return {
    type: Types.SELECTCONTACT,
    payload: contacts,
  };
};

export const dataPackes = data => {
  return {
    type: Types.DATAPACKES,
    payload: data,
  };
};

export const phoneNumber = mobileNumber => {
  return {
    type: Types.MOBILENUMBER,
    payload: mobileNumber,
  };
};
