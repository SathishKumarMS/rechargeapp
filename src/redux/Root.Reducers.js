import {combineReducers} from 'redux';
import rechargeReducers from './reducers/RechargeReducer';

const appReducer = combineReducers({
  app: rechargeReducers,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
