const assets = {
  xstream: require('./images/xstream.png'),
  icici: require('./images/iciciLogo.webp'),
  airtel: require('./images/airtel.jpg'),
  upi: require('./images/upi-icon.webp'),
  amazon: require('./images/amazon.jpg'),
  phonepay: require('./images/phonepe.png'),
  googlepay: require('./images/google.png'),
  paytm: require('./images/paytm.png'),
  sbi: require('./images/sbi.png'),
  hdfc: require('./images/hdfc.png'),
};

export default assets;
