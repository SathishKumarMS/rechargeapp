import React from 'react';
import {SafeAreaView, StatusBar, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import Store from './src/redux/Store';
import RootNavigation from './src/navigations/RootNavigation';
import * as HelperStyles from './src/utils/HelperStyles';
import Colors from './src/utils/Colors';

const App = () => {
  return (
    <SafeAreaView style={HelperStyles.screenContainer(Colors.white)}>
      <StatusBar barStyle={'light-content'} backgroundColor={Colors.black} />
      <Provider store={Store}>
        <NavigationContainer>
          <RootNavigation />
        </NavigationContainer>
      </Provider>
    </SafeAreaView>
  );
};

export default App;
